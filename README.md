# container

## 介绍
golang 运行时 依赖注入

## 快速开始

```go
package main
import "gitee.com/aurora-engine/container"
type Aaa struct {
	Bbb
	*Ccc
	Name string
}

type Bbb struct {
	Name string
	*Ccc
}

type Ccc struct {
	Name string
	*Ddd
}

type Ddd struct {
	Name string
	*Fff
}

type Eee struct {
	Name string
}

type Fff struct {
	Name string
	*Aaa
	*Eee
}

func main(){
	aaa := &Aaa{Name: "aaa"}
	bbb := &Bbb{Name: "bbb"}
	ccc := &Ccc{Name: "ccc"}
	ddd := &Ddd{Name: "ddd"}
	eee := &Eee{Name: "eee"}
	//fff := &Fff{Name: "fff"}
	space := container.NewSpace()
	space.Put("", aaa)
	space.Put("", bbb)
	space.Put("", ccc)
	space.Put("", ddd)
	space.Put("", eee)
	//space.Put("", fff)
	err := space.Start()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
}
```


