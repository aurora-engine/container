package nest_example

import (
	"gitee.com/aurora-engine/container"
	"testing"
)

type A struct {
	Name string
	*B
	*C
}

type B struct {
	Name string
	*D
	*E
}

type C struct {
	Name string
	*F
	*G
}

type D struct {
	Name string
}

type E struct {
	Name string
}

type F struct {
	Name string
	*H
}

type G struct {
	Name string
	*I
}

type H struct {
	Name string
	*J
}

type I struct {
	Name string
	*K
}

type J struct {
	Name string
	*A
}

type K struct {
	Name string
}

func TestNest(t *testing.T) {
	space := container.NewSpace()
	a := new(A)
	b := new(B)
	c := new(C)
	d := new(D)
	e := new(E)
	f := new(F)
	g := new(G)
	h := new(H)
	i := new(I)
	j := new(J)
	k := new(K)
	space.Put("", a)
	space.Put("", b)
	space.Put("", c)
	space.Put("", d)
	space.Put("", e)
	space.Put("", f)
	space.Put("", g)
	space.Put("", h)
	space.Put("", i)
	space.Put("", j)
	space.Put("", k)
	err := space.Start()
	if err != nil {
		t.Error(err.Error())
		return
	}
	t.Log()
}
